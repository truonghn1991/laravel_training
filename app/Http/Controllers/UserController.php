<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserEditRequest;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Support\Facades\Input;
use Log;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        $countUser = count($users);

        return view('user.index', compact('users', 'countUser'));
    }

    public function create()
    {
        return view('user.create');
    }

    public function store(UserRequest $request)
    {
        try {
            $user = new User();
            $user->name = trim($request->name);
            $user->email = trim($request->email);
            $user->password = bcrypt(trim($request->password));

            if ($user->save()) {
                return redirect()->route('user.index')->with('msg', 'Add success!');
            } else {
                return back();
            }
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('user.edit', compact('user'));
    }

    public function update(UserEditRequest $request, $id)
    {
        try {
            $user = User::findOrFail($id);
            $user->name = trim($request->name);
            $user->email = $user->email;
            if (Input::has('password')) {
                $user->password = bcrypt(trim($request->password));
            } else {
                $user->password = $user->password;
            }
            if ($user->update()) {
                return redirect()->route('user.index')->with('msg', 'Update success!');
            } else {
                return redirect()->route('user.edit', compact('user'))->with('err', 'Err!');
            }
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    public function del($id)
    {
        $user = User::findOrFail($id);
        if ($user->delete()) {
            return redirect()->route('user.index')->with('msg', 'Del success!');
        } else {
            return redirect()->route('user.index')->with('err', 'Err!');
        }
    }
}
