<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';
    public $timestamps = false;

    public function courses()
    {
        return $this->belongsToMany('App\Cource', 'course_id');
    }
}
