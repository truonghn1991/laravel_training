@extends('layouts.app')
@section('content')
	<h1>Users</h1>
	<a href="">Create</a>
	@if(Session::has('msg'))
        <div class="alert alert-success">
        	{{ Session::get('msg') }}
        </div>
    @endif
	@if ($users)
		<table class="table table-border">
			<thead>
				<tr>
					<th>Name</th>
					<th>Email</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
				<tr>
					<td>{{ $user->name }}</td>
					<td>{{ $user->email }}</td>
					<td>
	                    <a href="">Edit</a>
	                    <a href="">Del</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	@else
		<div>
			Looks like we haven't added any users, yet!
		</div>
	@endif
@stop
