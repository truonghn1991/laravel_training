@extends('layouts.app')
@section('content')
    <h1>Create User</h1>
    <form method="POST" action="{{ route('user.create') }}">
        {{ csrf_field() }}
        @if(Session::has('err'))
            <li>{{ Session::get('err') }}</li>
        @endif

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name"/>
        </div>
        @if ($errors->first('name'))
            <span>{{ $errors->first('name') }}</span>
        @endif

        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" name="email"/>
        </div>
        @if ($errors->first('email'))
        	<span>{{ $errors->first('email') }}</span>
        @endif

        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password"/>
        </div>
        @if ($errors->first('password'))
        	<span>{{ $errors->first('password') }}</span>
        @endif
        <input type="submit" value="Create User"/>
    </form>
@stop
