@extends('layouts.app')
@section('content')
    <h1>Edit User</h1>
    <form method="POST" action="{{ route('user.edit', $user->id) }}">
        {{ csrf_field() }}
        @if(Session::has('err'))
            <li>{{ Session::get('err') }}</li>
        @endif

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" value="{{ $user->name }}"/>
        </div>
        @if ($errors->first('name'))
            <span>{{ $errors->first('name') }}</span>
        @endif

        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" name="email" value="{{ $user->email }}" disabled />
        </div>

        <div class="form-group">
            <label for="password">Change Password</label>
            <input type="password" class="form-control" name="password" value="" />
        </div>
        <input type="submit" value="Update User"/>
    </form>
@stop
