<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::pattern('id', '([0-9]+)');

Route::get('/', function () {
    return view('welcome');
});

Route::group(['namespace' => 'User'], function () {
    Route::get('/users', 'UsersController@index');
});

Route::group(['prefix' => 'user'], function () {
    Route::get('/', ['uses' => 'UserController@index', 'as' => 'user.index']);
    Route::get('/create', ['uses' => 'UserController@create', 'as' => 'user.create']);
    Route::post('/create', ['uses' => 'UserController@store', 'as' => 'user.create']);
    Route::get('/edit/{id}', ['uses' => 'UserController@edit', 'as' => 'user.edit']);
    Route::post('/edit/{id}', ['uses' => 'UserController@update', 'as' => 'user.edit']);
    Route::get('/del/{id}', ['uses' => 'UserController@del', 'as' => 'user.del']);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
